# learning machines tools

import os
import sys
import glob
import json
import datetime
import pickle
import urllib.request 


import numpy as np
import pandas as pd
from sklearn import preprocessing
import matplotlib.pyplot as plt
from azure.storage.file import FileService
    

def pickle_to_file(obj, fname):
    path = os.path.join("plots/", fname+".pickle")      
    with open(path, 'wb') as handle:
        pickle.dump(obj, handle, protocol=pickle.HIGHEST_PROTOCOL)

def maybe_download(url, filename):
    if not os.path.isfile(filename):
        print("download ", filename)
        urllib.request.urlretrieve(url, filename)        
        

def get_run_name(prefix="run", additional=""):
    return "_".join([prefix, 
                     datetime.datetime.now().strftime("%Y-%m-%d-%H%M%S"),
                     additional])


def make_dir(new_dir):
    if not os.path.isdir(new_dir):
        os.mkdir(new_dir)


def indices_to_one_hot(data, nb_classes):
    """Convert an iterable of indices to one-hot encoded labels."""
    targets = np.array(data).reshape(-1)
    return np.eye(nb_classes)[targets]    


# copy data including all files and subdirectories from azure storage into local data-diretory
# skip if data are already loaded
def get_from_azure(file_service,
                   azure_fileshare,
                   source_dir,
                   target_dir):
        
    # if os.path.isdir(target_dir) and len(os.listdir(target_dir)) != 0 and False:
    #     print("data are already downloaded", len(os.listdir(target_dir)))
    print("make dir", target_dir)
    make_dir(target_dir)
    # generator = file_service.list_directories_and_files(azure_fileshare,source_dir, num_results=2000)
    generator = file_service.list_directories_and_files(azure_fileshare,source_dir)

    for file_or_dir in generator:
        if str(type(file_or_dir))[-6:-2] == 'File':
            target_path = os.path.join(target_dir, file_or_dir.name)
            print("target_path", target_path)
            if not os.path.isfile(target_path):
                file_service.get_file_to_path(azure_fileshare, source_dir, file_or_dir.name, target_path)                
        else:   # if directory, call function recursively
            make_dir(os.path.join(target_dir, file_or_dir.name))
            get_from_azure(file_service, 
                           azure_fileshare,
                           os.path.join(source_dir, file_or_dir.name),
                           os.path.join(target_dir, file_or_dir.name))


def get_data_from_azure(my_account_name = 'jochenstorage',
                        my_account_key = '7onD5l6X5dfmfUC+gXJCnKRn5AwPRXz8lEDaIsTVH5Di5y0wFTWgitc6Rq2TV85Zjx+EOMlRJYsxFHt1R41qPA==',
                        my_azure_fileshare = 'jochenfileshare',
                        source_dir = '', 
                        target_dir = './data/'):

    file_service = FileService(account_name=my_account_name, account_key=my_account_key)
    
    get_from_azure(file_service, my_azure_fileshare, source_dir, target_dir)
    

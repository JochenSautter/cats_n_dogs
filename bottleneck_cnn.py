# learning machines tools gaga
import os
import time
import numpy as np

from tensorflow.keras import applications, optimizers
from tensorflow.keras.preprocessing.image import ImageDataGenerator, load_img, img_to_array
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Conv2D, MaxPooling2D
from tensorflow.keras.layers import Input, Activation, Dropout, Flatten, Dense
from tensorflow.keras.regularizers import l1_l2

import tools as t
import model_tools as mt
import mymobilenet as mmn


'''This script is inspired by  the blog post https://blog.keras.io/building-powerful-image-classification-models-using-very-little-data.html

data/
    train/
        dogs/
            dog001.jpg
            ...
        cats/
            cat001.jpg
    validation/
        dogs/
        cats/
'''

        
class Bottleneck_CNN:
    def __init__(self,
                 model_type,
                 train_dir = 'data/train', 
                 valid_dir = 'data/validation',
                 test_dir = 'data/test',
                 max_train_samples = None,
                 max_valid_samples = None,
                 bottleneck_features_train_path = 'bottleneck_features_train.npy',
                 bottleneck_features_valid_path = 'bottleneck_features_valid.npy',
                 top_weights_path = 'top_weights.h5',
                 full_weights_path = 'full_weights.h5',
                 img_width = 150, img_height = 150):

        self.model_type = model_type
        self.train_dir = train_dir
        self.valid_dir = valid_dir
        self.test_dir = test_dir
        self.bottleneck_features_train_path = bottleneck_features_train_path
        self.bottleneck_features_valid_path = bottleneck_features_valid_path
        self.train_labels_path = 'train_labels.npy'
        self.valid_labels_path = 'valid_labels.npy'     # intermediate storing of labels
        self.top_weights_path = top_weights_path
        self.full_weights_path = full_weights_path
        self.img_width = img_width
        self.img_height = img_height

        self.L1L2 = False

        # self.num_classes = GetNumClasses(self.train_dir)
        self.num_classes = len(next(os.walk(self.train_dir))[1])
        print(self.num_classes, " classes detected under directory ", self.train_dir)

        self.max_train_samples = mt.samples_in_dir(self.train_dir) if max_train_samples == None else max_train_samples
        self.max_valid_samples = mt.samples_in_dir(self.valid_dir) if max_valid_samples == None else max_valid_samples
        print("training on ", self.max_train_samples, " training samples and ", self.max_valid_samples, " validation samples")

        self.train_labels = None
        self.valid_labels = None
        
        self.callbacks = mt.GetCallBacks(tensor_board = True)

        # self.train_labels = self.get_labels_from_generator(self.train_dir, self.max_train_samples)
        # self.valid_labels = self.get_labels_from_generator(self.valid_dir, self.max_valid_samples) 

        self.base_model = self.load_base_model()

        self.top_model = self.build_top_model(self.base_model.layers[-1].output.shape[1:])

        self.full_model = Model(inputs=self.base_model.input, outputs=self.top_model(self.base_model.output))
        print("built full model (base plus top)")
        
        # self.full_model = None  # built only if required


    def load_base_model(self, include_top = False):

        print("loading base model: ", self.model_type)

        if self.model_type == 'VGG16':
            base_model = applications.VGG16(weights='imagenet', include_top=include_top, 
                                            input_shape=(self.self.img_width, self.img_height, 3))

        elif self.model_type == 'MyMobileNet':
            print("loading custom MyMobileNet")
            base_model = mmn.MyMobileNet(weights='imagenet', include_top=include_top, 
                                                input_shape=(self.img_width, self.img_height, 3))

        elif self.model_type == 'MobileNet':
            base_model = applications.MobileNet(weights='imagenet', include_top=include_top, 
                                                  input_shape=(self.img_width, self.img_height, 3))        

        elif self.model_type == 'MobileNetV2':
            base_model = applications.MobileNetV2(weights='imagenet', include_top=include_top, 
                                                  input_shape=(self.img_width, self.img_height, 3))        
            
        elif self.model_type == 'ResNet50':
            # print("model type is ResNet50")
            base_model = applications.ResNet50(weights='imagenet', include_top=include_top, 
                                               input_shape=(self.img_width, self.img_height, 3))

        elif self.model_type == 'InceptionV3':
            base_model = applications.InceptionV3(weights='imagenet', include_top=include_top, 
                                                  input_shape=(self.img_width, self.img_height, 3))

        elif self.model_type == 'InceptionResNetV2':
            base_model = applications.InceptionResNetV2(weights='imagenet', include_top=include_top, 
                                                        input_shape=(self.img_width, self.img_height, 3))

        elif self.model_type == 'Xception':
            base_model = applications.Xception(weights='imagenet', include_top=include_top, 
                                               input_shape=(self.img_width, self.img_height, 3))        

        elif self.model_type == 'NASNetMobile':
            base_model = applications.NASNetMobile(weights='imagenet', include_top=include_top, 
                                                   input_shape=(self.img_width, self.img_height, 3))                

        elif self.model_type == 'NASNetLarge':
            base_model = applications.NASNetLarge(weights='imagenet', include_top=include_top, 
                                                  input_shape=(self.img_width, self.img_height, 3))          

        elif self.model_type == 'DenseNet121':
            base_model = applications.DenseNet121(weights='imagenet', include_top=include_top, 
                                                  input_shape=(self.img_width, self.img_height, 3))        

        elif self.model_type == 'DenseNet169':
            base_model = applications.DenseNet169(weights='imagenet', include_top=include_top, 
                                                  input_shape=(self.img_width, self.img_height, 3))          
            
        else:
            print("Error: no valid model type: ", self.model_type)
            return None

        return base_model

    def build_top_model(self, input_shape):

        if self.L1L2==True:
            # kernel_regularizer=l1_l2(l1=cfg['l1'], l2=cfg['l2'])
            kernel_regularizer=l1_l2(l1=0.1, l2=0.1)
            print("create top model using L1L2 regularisation")
        else: 
            kernel_regularizer = None

        # build a classifier model to put on top of the convolutional model
        '''
        top_model = Sequential()
        top_model.add(Flatten(input_shape=input_shape))
        top_model.add(Dense(256, activation='relu'), kernel_regularizer = kernel_regularizer)
        top_model.add(Dropout(0.5))
        # top_model.add(Dense(1, activation='sigmoid'))    # binary case
        top_model.add(Dense(self.num_classes, activation='softmax')) 
        '''
        inputs = Input(input_shape, name='topmodel_input')
        x = Flatten(name = 'flatten')(inputs)
        # inputs = Flatten(input_shape, name='topmodel_input')

        x = Dense(256, activation='relu', name = 'dense_gaga', kernel_regularizer = kernel_regularizer)(x)
        x = Dropout(0.5)(x)
        # top_model.add(Dense(1, activation='sigmoid'))    # binary case
        x = Dense(self.num_classes, name = 'dense_softmax', activation='softmax')(x)

        # top_model = Model(inputs=[cfgs_input, lcs_input], outputs=[main_output])
        top_model = Model(inputs = inputs, outputs = x, name = 'topmodel')

        print('built top_model')
        return(top_model)




    @staticmethod
    def freeze_bottom_layers(model, freeze_blocks = 1):

        def freeze_up_to(up_to_layer):

            index = None
            for idx, layer in enumerate(model.layers):
                if layer.name == up_to_layer:
                    index = idx
                    print("max frozen layer is ", up_to_layer, "with index", index, "out of ", len(model.layers))
                    break

            if index == None:
                print("no valid layer name: ", up_to_layer)

            for layer in model.layers[:index+1]:
                layer.trainable = False

            return


        if model.name == 'vgg16':
            blocks = ['block4_pool','block3_pool', 'block2_pool', 'block1_pool']
            freeze_up_to(blocks[min(freeze_blocks-1,len(blocks)-1)])
            # for deeper unfreezing take 'block3_pool', 'block2_pool', 'block1_pool'

        elif model.name[:10] == 'mobilenet_':    # take all mobilenet versions
            blocks = ['conv_pw_12_relu','conv_pw_11_relu', 'conv_pw_10_relu', 'conv_pw_9_relu', 'conv_pw_8_relu', 'conv_pw_7_relu',
                      'conv_pw_6_relu', 'conv_pw_5_relu', 'conv_pw_4_relu', 'conv_pw_4_relu', 'conv_pw_2_relu', 'conv_pw_1_relu']
            freeze_up_to(blocks[min(freeze_blocks-1,len(blocks)-1)])

        elif model.name[:11] == 'mobilenetv2':
            blocks = ['block_14_add','block_12_add', 'block_11_add', 'block_9_add', 'block_8_add', 'block_6_add', 'block_5_add']
            freeze_up_to(blocks[min(freeze_blocks-1,len(blocks)-1)])

        elif model.name == 'resnet50':
            blocks = ['add_14','add_13', 'add_12', 'add_11', 'add_10', 'add_9', 'add_8', 'add_7', 'add_6', 'add_5',
                      'add_4', 'add_3', 'add_2', 'add_1']
            freeze_up_to(blocks[min(freeze_blocks-1,len(blocks)-1)])        

        elif model.name == 'inception_v3':
            blocks = ['mixed9','mixed8', 'mixed7', 'mixed6', 'mixed5', 'mixed4', 'mixed3', 'mixed2', 'mixed1']
            freeze_up_to(blocks[min(freeze_blocks-1,len(blocks)-1)])        

        elif model.name == 'inception_resnet_v2':
            blocks = ['block8_9','block8_8', 'block8_4', 'block8_1', 'mixed_7a', 'block17_20', 'block17_10', 'block17_1']
            freeze_up_to(blocks[min(freeze_blocks-1,len(blocks)-1)])        

        elif model.name == 'xception':
            blocks = ['add_11','add_10', 'add_9', 'add_8', 'add_7', 'add_6', 'add_5', 'add_4', 'add_3', 'add_2', 'add_1']
            freeze_up_to(blocks[min(freeze_blocks-1,len(blocks)-1)])        

        elif model.name == 'densenet121':
            blocks =['conv5_block15_concat', 'conv5_block14_concat', 'conv5_block12_concat', 'conv5_block6_concat', 
                     'conv4_block15_concat', 'conv4_block14_concat', 'conv4_block12_concat', 'conv4_block6_concat',  
                     'conv3_block15_concat', 'conv3_block14_concat', 'conv3_block12_concat', 'conv3_block6_concat',  
                     'conv2_block15_concat', 'conv2_block14_concat', 'conv2_block12_concat', 'conv2_block6_concat']
            freeze_up_to(blocks[min(freeze_blocks-1,len(blocks)-1)])        

        elif model.name == 'densenet169':
            blocks =['conv5_block30_concat', 'conv5_block29_concat', 'conv5_block20_concat', 'conv5_block10_concat', 
                     'conv4_block30_concat', 'conv4_block29_concat', 'conv4_block20_concat', 'conv4_block10_concat', 
                     'conv3_block30_concat', 'conv3_block29_concat', 'conv3_block20_concat', 'conv3_block10_concat', 
                     'conv2_block30_concat', 'conv2_block29_concat', 'conv2_block20_concat', 'conv2_block10_concat']
            freeze_up_to(blocks[min(freeze_blocks-1,len(blocks)-1)])        

        elif model.name == 'NASNet':   # either NASNetLarge or NASNetMobile

            if len(model.layers) > 770:    # indicates Version == NASNetLarge 
                blocks =['normal_concat_17','normal_concat_16','normal_concat_15','normal_concat_14','normal_concat_13',
                         'normal_concat_12','normal_concat_11','normal_concat_10','normal_concat_9','normal_concat_8',
                         'normal_concat_7','normal_concat_6','normal_concat_5','normal_concat_4','normal_concat_3']
                freeze_up_to(blocks[min(freeze_blocks-1,len(blocks)-1)])        

            else:                                            # Version NASNetMobile
                blocks =['normal_concat_11','normal_concat_10','normal_concat_9','normal_concat_8', 'normal_concat_7', 
                         'normal_concat_6',  'normal_concat_5', 'normal_concat_4', 'normal_concat_3']
                freeze_up_to(blocks[min(freeze_blocks-1,len(blocks)-1)])        

        else:
            print("Error: model type not specified in function: ", model.name)

    
    def print_layers(model):
        total_params = 0
        for i, layer in enumerate(model.layers):
            total_params += layer.count_params()
            print(i, layer.name, '     ', layer.count_params(), 'weights, trainable',layer.trainable)
        print(total_params, 'weights in', i, 'layers')
        

    # pass train or valid data through cnn base model, and store resulting features (for later processing)
    def save_bn_features(self,data_dir, 
                         bn_features_path,
                         max_samples,
                         labels,
                         batch_size):

        print("now predict data from ", data_dir)  

        datagen = ImageDataGenerator(rescale=1. / 255)
        generator = datagen.flow_from_directory(
            data_dir,
            target_size=(self.img_width, self.img_height),
            batch_size=batch_size,
            class_mode=None,   # this means our generator will only yield batches of data, no labels
            shuffle=False)     # our data will be in order, so all first 1000 images will be cats, then 1000 dogs
        
        # get output of based model (cnn), given training or validation data
        bottleneck_features = self.base_model.predict_generator(
            generator, steps = max_samples // batch_size, verbose = 1)

        np.save(bn_features_path,bottleneck_features)
        print("features saved of ", bottleneck_features.shape[0], "samples from data directory:", data_dir, 
            "into file ", bn_features_path)

        # labels = t.indices_to_one_hot(generator.classes,self.num_classes)
        print("saving labels in one hot format")
        if data_dir == self.train_dir: 
            self.train_labels = t.indices_to_one_hot(generator.classes,self.num_classes)
            np.save(self.train_labels_path, self.train_labels)
        elif data_dir == self.valid_dir:        
            self.valid_labels = t.indices_to_one_hot(generator.classes,self.num_classes)
            np.save(self.valid_labels_path, self.valid_labels)


    def save_bottleneck_features(self, batch_size=16):

        self.base_model.compile(loss='categorical_crossentropy',
                                optimizer='rmsprop',
                                metrics=['accuracy'])

        self.save_bn_features(self.train_dir, self.bottleneck_features_train_path, self.max_train_samples, 
            self.train_labels, batch_size)
        self.save_bn_features(self.valid_dir, self.bottleneck_features_valid_path, self.max_valid_samples,
            self.valid_labels, batch_size)


    # train top model taking the saved features resulting from feeding training data through base_model(cnn) 
    def fit_top_model(self, config, epochs, reset_weights = False):

        if self.train_labels is None: 
            print("run save_bottleneck_features() first - now load last stored labels")
            self.train_labels = np.load(self.train_labels_path)
            self.valid_labels = np.load(self.valid_labels_path)

        train_data = np.load(self.bottleneck_features_train_path)
        valid_data = np.load(self.bottleneck_features_valid_path)


        print("now compile top model")
        if reset_weights:
            print("reset weights")
            mt.reset_model_weights(self.top_model)

        self.top_model.compile(loss='categorical_crossentropy',
                               optimizer=optimizers.SGD(lr = config['lr'], momentum=config['momentum']),
                               metrics=['accuracy'])    
        
        print("fit top model on training data with shape: ", train_data.shape)
        history = self.top_model.fit(train_data, self.train_labels[:train_data.shape[0]],
                epochs=epochs,
                batch_size=config['batch_size'],
                callbacks = self.callbacks,
                validation_data=(valid_data, self.valid_labels[:valid_data.shape[0]]))    

        self.top_model.save_weights(self.top_weights_path)
        print("weights saved as ", self.top_weights_path)
        
        return(history)

        
    # use pretrained base model, add top-model pretrained with fit_bottleneck_top(), 
    # unfreeze and train upper part of convolutional base model
    # !! check parameters of keras models - MobileNet allows specification of dropout rate and "alpha" !!
    def fit_base_model(self, config, 
                            load_weights = False, save_weights = True,   # full weights of model
                            freeze_blocks = 1, 
                            epochs = 25):

        # freeze lower layers up to leyer with name up_to_layer (None takes default)
        # to chooes approproate freezing limit freeze_up_to check model topology using plot_model_topology(model_type)
        # check result of freezing operation using print_layers(model)

        self.freeze_bottom_layers(self.base_model, freeze_blocks = freeze_blocks)

        # compile the model with a SGD/momentum optimizer and a very slow learning rate.
        self.full_model.compile(loss='categorical_crossentropy',
                    optimizer=optimizers.SGD(lr = config['lr'], momentum=config['momentum']),
                    metrics=['accuracy'])
        print('full model built and compiled')

        if load_weights:
            print("weights of full model loading from ", self.full_weights_path)   
            self.full_model.load_weights(self.full_weights_path)
        else:
            # note that it is necessary to start with a fully-trained
            # classifier, including the top classifier, in order to successfully do fine-tuning
            self.top_model.load_weights(self.top_weights_path)

        # prepare data augmentation configuration - test with 
        train_datagen = ImageDataGenerator(
            rescale=1. / 255,
            rotation_range=40,
            width_shift_range=0.2,
            height_shift_range=0.2, 
            # brightness_range=0.3,    causes error
            shear_range=0.2,
            zoom_range=0.2,
            horizontal_flip=True,
            fill_mode='nearest')

        train_generator = train_datagen.flow_from_directory(
            self.train_dir,
            target_size=(self.img_height, self.img_width),
            batch_size=config['batch_size'],
            class_mode='categorical')

        valid_datagen = ImageDataGenerator(rescale=1. / 255)

        valid_generator = valid_datagen.flow_from_directory(
            self.valid_dir,
            # save_to_dir='augmented',      # for optionally inspecting results of data augmentation 
            target_size=(self.img_height, self.img_width),
            batch_size=config['batch_size'],
            class_mode='categorical')


        # fine-tune the model
        print("fit model on ", self.max_train_samples, " training samples, ", self.max_valid_samples, " validation samples") 

        history = self.full_model.fit_generator(
            train_generator,
            steps_per_epoch=self.max_train_samples // config['batch_size'],    
            epochs=epochs,
            callbacks = self.callbacks,
            validation_data=valid_generator,
            validation_steps=self.max_valid_samples // config['batch_size'])
        
        if save_weights:
            print("weights of full model saving as ", self.full_weights_path)
            self.full_model.save_weights(self.full_weights_path)
        
        print('val_acc : ', history.history['val_acc'] )
        print('acc: ', history.history['acc'] )


    def predict_test(self, 
                    test_dir = None,
                    load_weights = False, 
                    max_test_samples = None):

        # if self.full_model == None:
        #     self.full_model = Model(inputs=self.base_model.input, outputs=self.top_model(self.base_model.output))

        if test_dir == None:
            test_dir = self.test_dir
    
        batch_size = 16

        max_test_samples = mt.samples_in_dir(test_dir) if max_test_samples == None else max_test_samples
        if load_weights:
            print("weights of full model loading from ", self.full_weights_path)   
            self.full_model.load_weights(self.full_weights_path)

        test_datagen = ImageDataGenerator(rescale=1. / 255)
        # test_datagen = ImageDataGenerator()        

        print("gaga")
        test_generator = test_datagen.flow_from_directory(
            test_dir,
            target_size=(self.img_height, self.img_width),
            batch_size=batch_size,
            class_mode='categorical')

        if self.full_model.optimizer == None:
            self.full_model.compile(loss='categorical_crossentropy', optimizer=optimizers.SGD())

        print("now predict")
        start_time = time.time()          
        preds = self.full_model.predict_generator(
            test_generator, 
            # steps = 2,
            steps=max_test_samples // batch_size, 
            verbose = 1)

        runtime = time.time() - start_time

        print("runtime for ", max_test_samples, "samples: ", runtime, runtime/max_test_samples, "per sample")

        return(preds)


    def predict_image(self, img_path, 
                    load_weights = False):

        if load_weights:
            print("weights of full model loading from ", self.full_weights_path)              
            self.full_model.load_weights(self.full_weights_path)

        img = load_img(img_path, target_size=(self.img_height, self.img_width))

        start_time = time.time()          
        x = img_to_array(img)  # this is a Numpy array with shape (3, 150, 150)
        x = x.reshape((1,) + x.shape)/255  # this is a Numpy array with shape (1, 3, 150, 150) 7 dont forget rescale !        
        pred = self.full_model.predict(x)

        runtime = time.time() - start_time
        print("runtime for one sample ", runtime)

        return(pred)


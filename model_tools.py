# learning machines tools

'''
import os
import sys
import glob
import json
import datetime
import pickle
import urllib.request 


import numpy as np
import pandas as pd
from sklearn import preprocessing
import matplotlib.pyplot as plt
from azure.storage.file import FileService
'''

import os
import numpy as np
from tensorflow.keras import backend as K
from tensorflow.keras.utils import plot_model
from tensorflow.keras.callbacks import EarlyStopping, LearningRateScheduler, Callback, TensorBoard



    
# get number of data-files in directory (including subdirectories)
def samples_in_dir(data_dir):
    return sum([len(files) for r, d, files in os.walk(data_dir)])


def reset_model_weights(model):
    session = K.get_session()
    for layer in model.layers: 
        if hasattr(layer, 'kernel_initializer'):
            layer.kernel.initializer.run(session=session)


def print_layers(model):
    total_params = 0
    for i, layer in enumerate(model.layers):
        total_params += layer.count_params()
        print(i, layer.name, '     ', layer.count_params(), 'weights, trainable',layer.trainable)
    print(total_params, 'weights in', i, 'layers')
        

# plot model topology into png file, for determining appropriate bottlenecks for unfreezing
def plot_model_topology(model):

    # my_model = load_base_model(model_type, 128, 128)   

    plot_file_name = model.name+'.png'
    print("plotting to file: ", plot_file_name)    
    plot_model(model, to_file=plot_file_name)


import matplotlib.pyplot as plt
def plot_weight_histogram(layer):

    # An "interface" to matplotlib.axes.Axes.hist() method
    
    d = np.random.laplace(loc=15, scale=3, size=500)

    n, bins, patches = plt.hist(x=d, bins='auto', color='#0504aa',
                                alpha=0.7, rwidth=0.85)
    plt.grid(axis='y', alpha=0.75)
    plt.xlabel('Value')
    plt.ylabel('Frequency')
    plt.title('My Very Own Histogram')
    plt.text(23, 45, r'$\mu=15, b=3$')
    maxfreq = n.max()
    # Set a clean upper y-axis limit.
    plt.ylim(top=np.ceil(maxfreq / 10) * 10 if maxfreq % 10 else maxfreq + 10)



'''
def get_labels_from_generator(self, data_dir, max_labels):

    # caution: dont reuse same data-generator twice, because after running it cannot be reset to position zero !
    datagen = ImageDataGenerator(rescale=1. / 255)    
    batch_size = 16    # should not matter
    generator = datagen.flow_from_directory(
        data_dir,
        target_size=(2, 2),
        batch_size=batch_size,
        class_mode='categorical',   # this means our generator will only yield batches of data, no labels
        shuffle=False)         # our data will be in order, so all first 1000 images will be cats, then 1000 dogs        

    labels = np.zeros([0,self.num_classes])   # initialise array with zero rows and 
    # print("created", labels)
    i,j = 0,0
    for next_batch in generator:
        if labels.shape[0] + batch_size > max_labels: 
            break
        # i += 1
        # j += next_batch[1].shape[0]
        # print("i,j",i,j)
        # print("type", type(next_batch))
        # print("type", type(next_batch[1]))
        # print("next labels batch [1]", next_batch[1])
        # print("next labels batch", next_batch)
        
        # labels = np.append(labels, next_batch[1])
        # print("labels now", labels[:5])
        labels = np.append(labels, next_batch[1], axis = 0)
        
        # print("current labels len", labels.shape[0])
    # print("len labels", labels.shape[0])            
    # print("labels", labels)
    return labels
'''

# schedule to be passed to MYLearningRateScheduler()
def exp_decay(epoch, lr, k_exp=1):
    
    initial_lrate = lr    
    k = k_exp  
    lrate = initial_lrate * np.exp(-k*epoch)
    if (epoch % 5 == 0):
        print("new lr: ", lrate)
    return lrate



# copied from https://github.com/keras-team/keras/blob/master/keras/callbacks.py#L591 
# and enhanced in order to facilitate passing parameter k_exp to schedule function exp_decay
class MyLearningRateScheduler(Callback):

    def __init__(self, schedule, init_lr, k_exp=0, verbose=0):
        super(MyLearningRateScheduler, self).__init__()
        self.schedule = schedule
        self.init_lr = init_lr        
        self.k_exp = k_exp     
        self.verbose = verbose

    def on_epoch_begin(self, epoch, logs=None):
        if not hasattr(self.model.optimizer, 'lr'):
            raise ValueError('Optimizer must have a "lr" attribute.')
        lr = float(K.get_value(self.model.optimizer.lr))
        lr = self.schedule(epoch, lr=self.init_lr, k_exp=self.k_exp)

        if not isinstance(lr, (float, np.float32, np.float64)):
            raise ValueError('The output of the "schedule" function '
                             'should be float.')
        K.set_value(self.model.optimizer.lr, lr)
        if self.verbose > 0:
            print('\nEpoch %05d: LearningRateScheduler reducing learning rate to %s.' % (epoch + 1, lr))

            
# stores weights of best values and restore these after training --> not done by default by earlystop !! 
# modified keras ModelCheckpoint class see https://github.com/keras-team/keras/issues/2768 code user louis925
# enhanced with "reset" parameter
class GetBest(Callback):
    """Get the best model at the end of training.
    # Arguments
        monitor: quantity to monitor.
        verbose: verbosity mode, 0 or 1.
        mode: one of {auto, min, max}.
            The decision
            to overwrite the current stored weights is made
            based on either the maximization or the
            minimization of the monitored quantity. For `val_acc`,
            this should be `max`, for `val_loss` this should
            be `min`, etc. In `auto` mode, the direction is
            automatically inferred from the name of the monitored quantity.
        period: Interval (number of epochs) between checkpoints.
    # Example
        callbacks = [GetBest(monitor='val_acc', verbose=1, mode='max')]
        mode.fit(X, y, validation_data=(X_eval, Y_eval),
                 callbacks=callbacks)
    """
    # reset best found value at beginning of each new training
    def __init__(self, monitor='val_loss', verbose=0,
                 mode='auto', period=1, reset = True):
        super(GetBest, self).__init__()
        self.monitor = monitor
        self.verbose = verbose
        self.period = period
        self.reset = reset
        self.best_epochs = 0
        self.epochs_since_last_save = 0

        if mode not in ['auto', 'min', 'max']:
            warnings.warn('GetBest mode %s is unknown, '
                          'fallback to auto mode.' % (mode),
                          RuntimeWarning)
            mode = 'auto'

        if mode == 'min':
            self.monitor_op = np.less
            self.best = np.Inf
        elif mode == 'max':
            self.monitor_op = np.greater
            self.best = -np.Inf
        else:
            if 'acc' in self.monitor or self.monitor.startswith('fmeasure'):
                # print("choose max as mode")
                self.monitor_op = np.greater
                self.best = -np.Inf
            else:
                # print("choose min as mode")                
                self.monitor_op = np.less
                self.best = np.Inf
                
    def on_train_begin(self, logs=None):
        if self.reset == True:      # useful if multiple calls of fit(), e.g. during cross validation 
            self.best = np.Inf if self.monitor_op == np.less else -np.Inf
        self.best_weights = self.model.get_weights()

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        self.epochs_since_last_save += 1
        if self.epochs_since_last_save >= self.period:
            self.epochs_since_last_save = 0
            #filepath = self.filepath.format(epoch=epoch + 1, **logs)
            current = logs.get(self.monitor)
            if current is None:
                warnings.warn('Can pick best model only with %s available, '
                              'skipping.' % (self.monitor), RuntimeWarning)
            else:
                if self.monitor_op(current, self.best):
                    if self.verbose > 0:
                        print('\nEpoch %05d: %s improved from %0.5f to %0.5f,'
                              ' storing weights.'
                              % (epoch + 1, self.monitor, self.best,
                                 current))
                    self.best = current
                    self.best_epochs = epoch + 1
                    self.best_weights = self.model.get_weights()
                else:
                    if self.verbose > 0:
                        print('\nEpoch %05d: %s is %0.5f, did not improve' %
                              (epoch + 1, self.monitor, current))            
                    
    def on_train_end(self, logs=None):
        if self.verbose > 0:
            print('Using epoch %05d with %s: %0.5f' % (self.best_epochs, self.monitor,
                                                       self.best))
        self.model.set_weights(self.best_weights)



def GetCallBacks(earlystop = False, lr_exp_decay = False, tensor_board = False):
    callbacks = []
    if earlystop==True:
        callbacks.append(EarlyStopping(monitor='val_loss', min_delta=0.00001,                                        
                                       patience=np.amin([np.amax([epochs/10, 5]),75]), 
                                       verbose=1, mode='auto'))
        # save weights at best iteration, and restore at end of training
        # reset = True --> takes best iteration for each CV-fold
        callbacks.append(GetBest(monitor='val_loss', verbose=1, mode='auto', reset = True))
        print("callback evaluating with early stopping")

    if lr_exp_decay==True:
        lrate = MyLearningRateScheduler(exp_decay, init_lr = cfg['lr'], k_exp=cfg['k_exp'])
        callbacks.append(lrate)
        print("callback evaluating with exponential decay")

    if tensor_board == True: 
        tb = TensorBoard(log_dir='./logs',   # https://www.tensorflow.org/api_docs/python/tf/keras/callbacks/TensorBoard
            histogram_freq=1, 
            batch_size=32, 
            write_graph=True, write_grads=True, write_images=True, 
            embeddings_freq=0, embeddings_layer_names=None, embeddings_metadata=None, embeddings_data=None)
            # update_freq='epoch')
        callbacks.append(tb)
        print("callback writing Tensorboard log file")

    return callbacks



# BOHB using https://github.com/automl/HpBandSter 

import numpy as np
import time

import ConfigSpace as CS
from hpbandster.core.worker import Worker

import logging
logging.basicConfig(level=logging.WARNING)

import tensorflow.keras.backend as K

import hpbandster.core.nameserver as hpns
import hpbandster.core.result as hpres
import hpbandster.utils

from hpbandster.optimizers import BOHB as BOHB

import bottleneck_cnn as m


class MyWorker(Worker):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        

    @staticmethod        
    def configuration_space_from_raw(hpRaw, hpRawConditions, resolve_multiple='AND'):

        cs = CS.ConfigurationSpace()
        #
        # add hyperparameters
        #
        for hp in hpRaw:
            if hp[4] == "float":
                cs.add_hyperparameter(
                    CS.UniformFloatHyperparameter(
                        hp[0],
                        lower=hp[1][0],
                        upper=hp[1][1],
                        default_value=hp[2],
                        log=hp[3]
                    )
                )
            elif hp[4] == "int":
                cs.add_hyperparameter(
                    CS.UniformIntegerHyperparameter(
                        hp[0],
                        lower=hp[1][0],
                        upper=hp[1][1],
                        default_value=hp[2],
                        log=hp[3]
                    )
                )
            elif (hp[4] == "cat"):
                cs.add_hyperparameter(
                    CS.CategoricalHyperparameter(
                        hp[0],
                        hp[1]
                    )
                )
            else:
                raise Exception("unknown hp type in hpRawList")

        #
        # add conditions
        #
        covered_conditions = dict()
        for cond in hpRawConditions:
            # check if conditions for that hyperparameter were already processed
            if cond[0] in covered_conditions:
                continue
            covered_conditions[cond[0]] = True

            # get all conditions for that hyperparameter
            all_conds_for_hyperparameter = []
            for other_cond in hpRawConditions:
                if other_cond[0] == cond[0]:
                    all_conds_for_hyperparameter.append(other_cond)

            # create the condition objects
            condition_objects = []
            for cond in all_conds_for_hyperparameter:
                if cond[1] == "eq":
                    condition_objects.append(
                        CS.EqualsCondition(
                            cs.get_hyperparameter(cond[0]),
                            cs.get_hyperparameter(cond[2]),
                            cond[3]))
                elif cond[1] == "gtr":
                    condition_objects.append(
                        CS.GreaterThanCondition(
                            cs.get_hyperparameter(cond[0]),
                            cs.get_hyperparameter(cond[2]),
                            cond[3]))
                else:
                    raise Exception("unknown condition type in hpRawConditions")

            # add the conditons to the configuration space
            if len(condition_objects) == 1:
                # simply add the condition
                cs.add_condition(condition_objects[0])
            else:
                # resolve multiple conditions
                if resolve_multiple == 'AND':
                    cs.add_condition(
                        CS.AndConjunction(*condition_objects))
                elif resolve_multiple == 'OR':
                    cs.add_condition(
                        CS.OrConjunction(*condition_objects))
                else:
                    raise Exception("resolve_multiple=", resolve_multiple, ". should be 'AND' or 'OR'")

        return cs
    
    
    # edit here for config-spaces (evtl. add new "model_type") 
    @classmethod
    def get_configspace(cls, model_type, lr_exp_decay=False):

            #<    name              >   <  Range       >      <Default>     <Log>   <Type>

        if model_type == 'ridge':
            hpRaw = [
                ['alpha',                     [0.1, 10],            1.0,        True,   'float'],
            ]

        elif model_type == 'xgb':
            hpRaw = [
                ['n_estimators',             [30, 300],          100,           False,  'int'],            
                ['lr',                       [0.001, 1.0],       0.3,           True,   'float'],
                ['gamma',                    [0.000, 1.0],       0.001,         False,   'float'],
                ['subsample',                [0.001, 1.0],       1.0,           False,   'float'],
                ['cols_bt',                  [0.001, 1.0],       1.0,           False,   'float'],
                ['maxdepth',                 [3., 10.],          6.0,           False,   'int'],             
            ]

        elif model_type == 'mlp':
            if lr_exp_decay == True:
                hpRaw = [
                    ['lr',                       [0.02, 0.2],     0.2,            True,    'float'],
                    ['k_exp',                    [0.001,0.1],     0.04,           True,    'float'],
                    ['batch_size',               [16, 32],         32,            True,    'int'],            
                ]
            else:
                hpRaw = [
                    ['lr',                       [0.001, 0.5],     0.05,          True,    'float'],
                    ['momentum',                 [0.5, 0.99],       0.9,          True,    'float'],                    
                    ['batch_size',               [16, 64],          32,           True,    'int'],
                ] 

        elif model_type == 'pretrained_cnn':
            hpRaw = [
                #<    name              >   <  Range       >   <Default>     <Log>   <Type>
                ['lr',                       [1e-6, 1e-3],      1e-5,         True,    'float'],
                ['momentum',                 [0.5, 0.99],       0.9,          True,    'float'],  
                ['batch_size',               [16, 64],          32,           True,    'int'],
                ['dropout',                  [16, 64],          32,           True,    'int'], # only implemented für MobileNet
            ]

        elif model_type == 'lstm' or model_type == 'multi_lstm':
            hpRaw = [
                ['lr',                           [0.001, 0.5],     0.05,           True,    'float'],
                ['batch_size',                   [16, 48],           24,           True,    'int'],
            ] 

        else:
            print("invalid model type: ", model_type)

        hpRawConditions = [
            #< conditional hp name >      <cond. Type>        <cond. variable>     <cond. value>
            # ["num_dense_units_1",           "gtr",          "num_dense_layers",     1],
            # ["num_dense_units_2",           "gtr",          "num_dense_layers",     2],
            # ["num_dense_units_3",           "eq",           "num_dense_layers",     4],
        ]

        return cls.configuration_space_from_raw(hpRaw, hpRawConditions, resolve_multiple='AND')    

    # plug in function to be run
    def compute(self, config_id, config, budget, working_directory, *args, **kwargs):
        try:
            print("worker computes ", config)
            start_time = time.time()    

            # class must be instantiated during each compute(), error otherwise. 
            bn = m.Bottleneck_CNN("MobileNet", img_width = 128, img_height = 128) 
            # bn = self.bottleneck

            print("now train on epochs: ", int(budget))
            history = bn.fit_top_model(config=config, epochs=int(budget), reset_weights = True)
            val_acc = history.history['val_acc'][-1]
            # history = self.objective_func_wrapper(
            #     config, epochs=int(budget), 
            #     *args, **kwargs)
            runtime = time.time() - start_time
            
        finally:
            print("clear Tensorflow session")
            K.clear_session()  # avoids problems with multithreading
        return ({
                'loss': 1-val_acc, # remember: HpBandSter always minimizes!
                'info': {"runtime": runtime,
                         "val_loss": history.history['val_loss'][-1]}
                })            

    # dummy compute() func to figure out the budgets resulting from chosen min_budget, max_budget, n_iterations
    '''
    def compute(self, config_id, config, budget, working_directory, *args, **kwargs):
        print("now train on epochs: ", int(budget))
        return ({ 'loss': 0.2, 'info': {"runtime": 1, "val_loss": 0.2}})            
    '''
    
    
    
def optimize():
    
    # see intro_hyper_opt.pdf, last slides for algorithm wrt min_budget, max_budget, n_iterations
    # n_iterations, outer loops, is "s_max" in the algorithm or 5 in the example on the table (second last slide)
    # run optimize() using the dummy compute() function to figure out the resulting budgets
    min_budget = 1              # should yield "reasonable" results - indicative for the final performance (often 1)
    max_budget = 16             # number of epochs run in the largest runs of hyberband
    n_iterations = int(np.log(max_budget / min_budget)/np.log(3))  # use log3 because hyperband uses successive tripling
                       
    print("successive halfing in n_iterations", n_iterations)
    
    run_id = 'example1'
    nic_name = 'lo'
    shared_directory= '.'
    
    host = hpns.nic_name_to_host(nic_name)
    
    # for analysis see https://automl.github.io/HpBandSter/build/html/auto_examples/plot_example_6_analysis.html
    result_logger = hpres.json_result_logger(directory=shared_directory, overwrite=True)    
    
    # Start a nameserver:
    NS = hpns.NameServer(run_id=run_id, host=host, port=0, working_directory=shared_directory)
    ns_host, ns_port = NS.start()

    myworker = MyWorker(run_id=run_id, host=host, nameserver=ns_host, nameserver_port=ns_port, timeout=120)
    myworker.run(background=True)

    # BOHB runs compute() as defined above in many runs, applying successive halving 
    # and for each run selects configspace parameters in an intelligent manner based on results of runs
    bohb = BOHB(  configspace = myworker.get_configspace('mlp'),
                  run_id = run_id, 
                  host = host,
                  nameserver=ns_host,
                  nameserver_port=ns_port,
                  result_logger=result_logger,
                  min_budget=min_budget, max_budget=max_budget
               )
    res = bohb.run(n_iterations=n_iterations)

    print("shutdown bohb")
    bohb.shutdown(shutdown_workers=True)
    NS.shutdown()   # !!? eventually problem, because cannot be shut down 

    # see https://automl.github.io/HpBandSter/build/html/core/result.html#hpbandster.core.result.Result
    id2config = res.get_id2config_mapping()
    incumbent = res.get_incumbent_id()
    
    if incumbent != None:
        print('Best found configuration:', id2config[incumbent]['config'])
        print('A total of %i unique configurations where sampled.' % len(id2config.keys()))
        print('A total of %i runs where executed.' % len(res.get_all_runs()))
        print('Total budget corresponds to %.1f full function evaluations.'%(sum([r.budget for r in res.get_all_runs()])/max_budget))
        # return(id2config[incumbent]['config'])
        return(res)
        
    else:
        print("no incumbent found, no run on largest budget performed")
        traj = res.get_incumbent_trajectory(all_budgets=True, bigger_is_better=True, non_decreasing_budget=True)        
        return(traj)

    
# test implementation of MyWorker quickly
def test_MyWorker():
    worker = MyWorker(run_id='0')
    cs = worker.get_configspace('mlp')

    config = cs.sample_configuration().get_dictionary()
    print(config)
    # see https://github.com/automl/HpBandSter/blob/master/hpbandster/core/worker.py for the meaning of config_id
    res = worker.compute(config_id = (1,0,0), config=config, budget=1, working_directory='.')
    print(res)
    return(res)
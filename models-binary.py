# learning machines tools
import os
import numpy as np

from tensorflow.keras import applications, optimizers
from tensorflow.keras import backend as K
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Conv2D, MaxPooling2D
from tensorflow.keras.layers import Activation, Dropout, Flatten, Dense
from tensorflow.keras.utils import plot_model

import tools as t

'''This script goes along the blog post https://blog.keras.io/building-powerful-image-classification-models-using-very-little-data.html

data/
    train/
        dogs/
            dog001.jpg
            ...
        cats/
            cat001.jpg
    validation/
        dogs/
        cats/
'''


def build_simple_cnn(img_width = 150, img_height = 150):

    # dimensions of our images.
    # img_width, img_height = 150, 150

    if K.image_data_format() == 'channels_first':
        input_shape = (3, img_width, img_height)
    else:
        input_shape = (img_width, img_height, 3)

    model = Sequential()
    model.add(Conv2D(32, (3, 3), input_shape=input_shape))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(64, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())
    model.add(Dense(64))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1))
    model.add(Activation('sigmoid'))

    model.compile(loss='binary_crossentropy',
                  optimizer='rmsprop',
                  metrics=['accuracy'])

    return model


def fit_simple_cnn(model, img_width = 150, img_height = 150):

    train_dir = 'data/train'
    valid_dir = 'data/validation'
    max_train_samples = 2000
    max_valid_samples = 800
    epochs = 50
    batch_size = 16

    # this is the augmentation configuration we will use for training
    train_datagen = ImageDataGenerator(rescale=1. / 255,
                                       shear_range=0.2,
                                       zoom_range=0.2,
                                       horizontal_flip=True)

    # this is the augmentation configuration we will use for testing:
    # only rescaling
    test_datagen = ImageDataGenerator(rescale=1. / 255)

    train_generator = train_datagen.flow_from_directory(train_dir,
                                                        target_size=(img_width, img_height),
                                                        batch_size=batch_size,
                                                        class_mode='binary')

    validation_generator = test_datagen.flow_from_directory(valid_dir,
                                                            target_size=(img_width, img_height),
                                                            batch_size=batch_size,
                                                            class_mode='binary')

    model.fit_generator(train_generator,
                        steps_per_epoch=max_train_samples // batch_size,
                        epochs=epochs,
                        validation_data=validation_generator,
                        validation_steps=max_valid_samples // batch_size)

    model.save_weights('first_try.h5')


def build_top_model(input_shape):
    # build a classifier model to put on top of the convolutional model
    top_model = Sequential()
    top_model.add(Flatten(input_shape=input_shape))
    top_model.add(Dense(256, activation='relu'))
    top_model.add(Dropout(0.5))
    top_model.add(Dense(1, activation='sigmoid'))
    print('built top_model')
    return(top_model)


def get_labels_from_generator(data_dir, batch_size, max_labels):

    # caution: dont reuse same data-generator twice, because after running it cannot be reset to position zero !
    datagen = ImageDataGenerator(rescale=1. / 255)    
    
    generator = datagen.flow_from_directory(
        data_dir,
        target_size=(8, 8),
        batch_size=batch_size,
        class_mode="binary",   # this means our generator will only yield batches of data, no labels
        shuffle=False)         # our data will be in order, so all first 1000 images will be cats, then 1000 dogs        

    labels = np.array([])
    # i,j = 0,0
    for next_batch in generator:
        if labels.shape[0] + batch_size > max_labels: 
            break
        # i += 1
        # j += next_batch[1].shape[0]
        # print("i,j",i,j)
        # print("next labels batch", next_batch[1])                  
        labels = np.append(labels, next_batch[1])
        # print("current labels len", labels.shape[0])
    print("len labels", labels.shape[0])            
    # print("labels", labels)

    return labels


def load_base_model(model_type, img_width, img_height, include_top = False):
    
    print("loading model: ", model_type)
    if model_type == 'VGG16':
        base_model = applications.VGG16(weights='imagenet', include_top=include_top, input_shape=(img_width, img_height, 3))

    elif model_type == 'MobileNet':
        base_model = applications.MobileNet(weights='imagenet', include_top=include_top, input_shape=(img_width, img_height, 3))

    elif model_type == 'MobileNetV2':
        base_model = applications.MobileNetV2(weights='imagenet', include_top=include_top, input_shape=(img_width, img_height, 3))        
        
    elif model_type == 'ResNet50':
        # print("model type is ResNet50")
        base_model = applications.ResNet50(weights='imagenet', include_top=include_top, input_shape=(img_width, img_height, 3))

    elif model_type == 'InceptionV3':
        base_model = applications.InceptionV3(weights='imagenet', include_top=include_top, input_shape=(img_width, img_height, 3))

    elif model_type == 'InceptionResNetV2':
        base_model = applications.InceptionResNetV2(weights='imagenet', include_top=include_top, input_shape=(img_width, img_height, 3))

    elif model_type == 'Xception':
        base_model = applications.Xception(weights='imagenet', include_top=include_top, input_shape=(img_width, img_height, 3))        

    elif model_type == 'NASNetMobile':
        base_model = applications.NASNetMobile(weights='imagenet', include_top=include_top, input_shape=(img_width, img_height, 3))                

    elif model_type == 'NASNetLarge':
        base_model = applications.NASNetLarge(weights='imagenet', include_top=include_top, input_shape=(img_width, img_height, 3))          

    elif model_type == 'DenseNet121':
        base_model = applications.DenseNet121(weights='imagenet', include_top=include_top, input_shape=(img_width, img_height, 3))        

    elif model_type == 'DenseNet169':
        base_model = applications.DenseNet169(weights='imagenet', include_top=include_top, input_shape=(img_width, img_height, 3))          
        
    else:
        print("Error: no valid model type: ", model_type)
        return None

    return base_model



def freeze_bottom_layers(model, up_to_layer = None):

    def freeze_up_to(up_to_layer, up_to_default):
        
        if up_to_layer == None:
            up_to_layer = up_to_default
            
        layer = model.get_layer(up_to_layer)
        
        index = None
        for idx, layer in enumerate(model.layers):
            if layer.name == up_to_layer:
                index = idx
                print("max frozen layer is ", up_to_layer, "with index", index)
                break
        
        if index == None:
            print("no valid layer name: ", up_to_layer)
        
        for layer in model.layers[:index+1]:
            layer.trainable = False
        
        return

    
    if model.name == 'vgg16':
        freeze_up_to(up_to_layer, 'block4_pool')
        # for deeper unfreezing take 'block3_pool', 'block2_pool', 'block1_pool'

    elif model.name[:11] == 'mobilenetv2':
        freeze_up_to(up_to_layer, 'block_14_add')
        # for deeper unfreezing take 'block_12_add', 'block_11_add', 'block_9_add' (not 13,10...)
        
    elif model.name[:9] == 'mobilenet':    # take all mobilenet versions
        freeze_up_to(up_to_layer, 'conv_pw_12_relu')
        # for deeper unfreezing take 'conv_pw_11_relu', 'conv_pw_10_relu', ....

    elif model.name == 'resnet50':
        freeze_up_to(up_to_layer, 'add_14')
        # for deeper unfreezing take 'add_13', 'add_12', ....

    elif model.name == 'inception_v3':
        freeze_up_to(up_to_layer, 'mixed9')
        # for deeper unfreezing take 'mixed8', 'mixed7', ....

    elif model.name == 'inception_resnet_v2':
        freeze_up_to(up_to_layer, 'block8_9')
        # for deeper unfreezing take 'block8_8', 'block8_1', 'mixed_7a', 'block17_20' and from 20 downwards...

    elif model.name == 'xception':
        freeze_up_to(up_to_layer, 'add_11')
        # for deeper unfreezing take 'add_10', 'add_9', ....

    elif model.name == 'NASNet':   # either NASNetLarge or NASNetMobile

        if len(model.layers) > 770:    # indicates Version == NASNetLarge 
            freeze_up_to(up_to_layer, 'normal_concat_17')
            # for deeper unfreezing take 'normal_concat_16', 'normal_concat_15', ....
        else:                                            # Version NASNetMobile
            freeze_up_to(up_to_layer, 'normal_concat_11')
            # for deeper unfreezing take 'normal_concat_10', 'normal_concat_9', ....

    elif model.name == 'nasnetlarge':
        freeze_up_to(up_to_layer, 'normal_concat_17')
        # for deeper unfreezing take 'normal_concat_16', 'normal_concat_15', ....

    elif model.name == 'densenet121':
        freeze_up_to(up_to_layer, 'conv5_block15_concat')
        # for deeper unfreezing take 'conv5_block14_concat', 'conv5_block1_concat', (for complete upper of 5 blocks of layers)

    elif model.name == 'densenet169':
        freeze_up_to(up_to_layer, 'conv5_block31_concat')
        # for deeper unfreezing take 'conv5_block30_concat', 'conv5_block1_concat', (for complete upper of 5 blocks of layers)
        
    else:
        print("Error: model type not specified in function: ", model.name)


# plot model topology into png file, for determining appropriate bottlenecks for unfreezing
def plot_model_topology(model_type):

    my_model = load_base_model(model_type, 128, 128)   

    plot_file_name = model_type+'.png'
    print("plotting to file: ", plot_file_name)    
    plot_model(my_model, to_file=plot_file_name)

def print_layers(model):
    for i, layer in enumerate(model.layers):
        print(i, layer.name, ', layer.trainable',layer.trainable)    
    

# pass train or valid data through cnn base model, and store resulting features (for later processing)
def save_bottleneck_features(model_type, 
                             data_dir, 
                             bn_features_path,
                             max_samples,
                             batch_size, 
                             img_width, img_height):
    
    datagen = ImageDataGenerator(rescale=1. / 255)

    base_model = load_base_model(model_type, img_width, img_height)        
    base_model.compile(loss='binary_crossentropy',
                       optimizer='rmsprop',
                       metrics=['accuracy'])
    
    print("now predict data from ", data_dir)  
    generator = datagen.flow_from_directory(
        data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode=None,   # this means our generator will only yield batches of data, no labels
        # class_mode="binary",   # this means our generator will only yield batches of data, no labels
        shuffle=False)     # our data will be in order, so all first 1000 images will be cats, then 1000 dogs
    
    # get output of based model (cnn), given training or validation data
    bottleneck_features = base_model.predict_generator(
        generator, steps = max_samples // batch_size, verbose = 1)
    
    np.save(bn_features_path,bottleneck_features)
    print("features saved of ", bottleneck_features.shape[0], "samples from data directory:", data_dir, 
          "into file ", bn_features_path)
    

# train top model taking the saved features resulting from feeding training data trough base_model(cnn) 
def train_top_model(train_labels, validation_labels, top_weights_path, 
                    bn_train_features_path, bn_valid_features_path,
                    batch_size, epochs):
    
    train_data = np.load(bn_train_features_path)
    # fc_train_labels = np.array([0] * (nb_train_samples // 2) + [1] * (nb_train_samples // 2))

    validation_data = np.load(bn_valid_features_path)
    # fc_validation_labels = np.array([0] * (nb_validation_samples // 2) + [1] * (nb_validation_samples // 2))

    model = build_top_model(train_data.shape[1:])

    print("now compile")       
    model.compile(optimizer='rmsprop',
                  loss='binary_crossentropy', metrics=['accuracy'])

    model.fit(train_data, train_labels[:train_data.shape[0]],
              epochs=epochs,
              batch_size=batch_size,
              validation_data=(validation_data, validation_labels[:validation_data.shape[0]]))    
    model.save_weights(top_weights_path)
    print("weights saved as ", top_weights_path)
    


# get number of data-files in directory (including subdirectories)
def samples_in_dir(data_dir):
    return sum([len(files) for r, d, files in os.walk(data_dir)])


def fit_bottleneck_top(
    train_dir = 'data/train', valid_dir = 'data/validation', 
    top_weights_path = 'top_weights.h5', 
    model_type = 'VGG16',    
    max_train_samples = 0, max_valid_samples = 0,
    batch_size = 16, epochs = 50,
    img_width = 150, img_height = 150):

    
    max_train_samples = max(samples_in_dir(train_dir), max_train_samples)
    max_valid_samples = max(samples_in_dir(valid_dir), max_valid_samples)
    print(max_train_samples, " training files found, ", max_valid_samples, " validation files found")

    save_bottleneck_features(model_type, train_dir, 'bottleneck_features_train.npy', 
                             max_train_samples, batch_size, 
                             img_width = img_width, img_height = img_height)

    save_bottleneck_features(model_type, valid_dir, 'bottleneck_features_valid.npy', 
                             max_valid_samples, batch_size, 
                             img_width = img_width, img_height = img_height)
    
    train_labels = get_labels_from_generator(train_dir, batch_size, max_train_samples)
    valid_labels = get_labels_from_generator(valid_dir, batch_size, max_valid_samples) 
    print(train_labels.shape[0], " train_labels")
    print(valid_labels.shape[0], " validation_labels")

    train_top_model(train_labels, valid_labels, 
                    top_weights_path, 'bottleneck_features_train.npy', 'bottleneck_features_valid.npy', 
                    batch_size, epochs)

    
# use pretrained base model, add top-model pretrained with fit_bottleneck_top(), 
# unfreeze and train upper part of convolutional base model
def fit_bottleneck_bottom(
    train_dir = 'data/train', valid_dir = 'data/validation',  
    top_weights_path = 'top_weights.h5',
    load_weights_path = None, save_weights_path = None, # paths of weigts to be loaded before training / saved after training
    model_type = 'VGG16',
    freeze_up_to = None,
    max_train_samples = 0, max_valid_samples = 0,
    batch_size = 16, epochs = 25,
    img_width = 150, img_height = 150):

    base_model = load_base_model(model_type, img_width, img_height)
    print('Base_Model loaded.', base_model.name)    

    # freeze lower layers up to leyer with name up_to_layer (None takes default)
    # to chooes approproate freezing limit check model topology using plot_model_topology(model_type)
    # check result of freezing operation using print_layers(model)
    freeze_bottom_layers(base_model, up_to_layer = freeze_up_to)

    # build a classifier model to put on top of the convolutional model
    top_model = build_top_model(base_model.output_shape[1:])

    # note that it is necessary to start with a fully-trained
    # classifier, including the top classifier, in order to successfully do fine-tuning
    top_model.load_weights(top_weights_path)

    # add the model on top of the convolutional base
    full_model = Model(inputs=base_model.input, outputs=top_model(base_model.output))
    print('Full Model built')
    # print_layers(full_model)   # to inspect proper unfreezing 
    
    # compile the model with a SGD/momentum optimizer and a very slow learning rate.
    full_model.compile(loss='binary_crossentropy',
                optimizer=optimizers.SGD(lr=1e-4, momentum=0.9),
                metrics=['accuracy'])
    print('full model compiled')

    if load_weights_path != None:
        full_model.load_weights(load_weights_path)
        print("weights of full model loaded from ", load_weights_path)    
    
    # prepare data augmentation configuration
    train_datagen = ImageDataGenerator(
        rescale=1. / 255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)

    train_generator = train_datagen.flow_from_directory(
        train_dir,
        target_size=(img_height, img_width),
        batch_size=batch_size,
        class_mode='binary')

    valid_datagen = ImageDataGenerator(rescale=1. / 255)

    valid_generator = valid_datagen.flow_from_directory(
        valid_dir,
        target_size=(img_height, img_width),
        batch_size=batch_size,
        class_mode='binary')

    # max_train_samples = determine_max_samples(train_dir, max_train_samples)
    # max_valid_samples = determine_max_samples(valid_dir, max_valid_samples)
    max_train_samples = max(samples_in_dir(train_dir), max_train_samples)
    max_valid_samples = max(samples_in_dir(valid_dir), max_valid_samples)
    print("fit model on ", max_train_samples, " training samples, ", max_valid_samples, " validation samples") 

    # fine-tune the model
    history = full_model.fit_generator(
        train_generator,
        # samples_per_epoch=nb_train_samples,
        steps_per_epoch=max_train_samples // batch_size,    
        epochs=epochs,
        validation_data=valid_generator,
        validation_steps=max_valid_samples // batch_size)
    
    if save_weights_path != None:
        full_model.save_weights(save_weights_path)
        print("weights of full model saved as ", save_weights_path)
    
    print('val_acc : ', history.history['val_acc'] )
    print('acc: ', history.history['acc'] )